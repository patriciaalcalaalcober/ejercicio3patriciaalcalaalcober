package ejercicio3;

import java.util.Scanner;

public class Ejercicio3 {

	public static void main(String[] args) {
		Scanner teclado=new Scanner(System.in);
		
		System.out.println("Escribe una frase");
		String frase=teclado.nextLine();
		System.out.println("La frase en minúsculas es "+frase.toLowerCase());
		teclado.close();

	}

}
